Android Versioning
===============

[![CircleCI](https://circleci.com/bb/android-universum/versioning.svg?style=shield)](https://circleci.com/bb/android-universum/versioning)
[![Codecov](https://codecov.io/bb/android-universum/versioning/branch/main/graph/badge.svg)](https://codecov.io/bb/android-universum/versioning)
[![Codacy](https://api.codacy.com/project/badge/Grade/9cadff300e1e4e5bbcbda5ded7c913e3)](https://www.codacy.com/app/universum-studios/versioning?utm_source=android-universum@bitbucket.org&amp;utm_medium=referral&amp;utm_content=android-universum/versioning&amp;utm_campaign=Badge_Grade)
[![Android](https://img.shields.io/badge/android-9.0-blue.svg)](https://developer.android.com/about/versions/pie/android-9.0)
[![Kotlin](https://img.shields.io/badge/kotlin-1.5.10-blue.svg)](https://kotlinlang.org)
[![Robolectric](https://img.shields.io/badge/robolectric-4.5.1-blue.svg)](http://robolectric.org)
[![Android Jetpack](https://img.shields.io/badge/Android-Jetpack-brightgreen.svg)](https://developer.android.com/jetpack)

Simple tracking of application versions for the Android platform.

For more information please visit the **[Wiki](https://bitbucket.org/android-universum/versioning/wiki)**.

## Download ##

Download the latest **[release](https://bitbucket.org/android-universum/versioning/addon/pipelines/deployments "Deployments page")** or **add as dependency** in your project via:

### Gradle ###

    implementation "io.bitbucket.android-universum:versioning:${DESIRED_VERSION}@aar"

## Modules ##

This library may be used via **separate [modules](https://bitbucket.org/android-universum/versioning/src/main/MODULES.md)**
in order to depend only on desired _parts of the library's code base_ what ultimately results in **fewer dependencies**.

## Compatibility ##

Supported down to the **Android [API Level 14](http://developer.android.com/about/versions/android-4.0.html "See API highlights")**.

### Dependencies ###

- [`androidx.annotation:annotation`](https://developer.android.com/jetpack/androidx)

## [License](https://bitbucket.org/android-universum/versioning/src/main/LICENSE.md) ##

**Copyright 2021 Universum Studios**

_Licensed under the Apache License, Version 2.0 (the "License");_

You may not use this file except in compliance with the License. You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software distributed under the License
is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
or implied.
     
See the License for the specific language governing permissions and limitations under the License.