/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.version

import androidx.annotation.NonNull
import java.util.regex.Pattern

/**
 * Represents an application version with a **semantic** name. New version may be simply created via
 * [Version.create] providing the desired version name.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @constructor Creates a new version of Version with the specified `name`.
 * @param name Desired name for the new version.
 */
class Version private constructor(
        /**
         * The name of this version.
         */
        @NonNull val name: String) : Comparable<Version> {

    /*
     * Companion ===================================================================================
     */

    /**
     */
    companion object {

        /**
         * Empty instance (NULL object) which does not have any name specified.
         */
        @JvmStatic @NonNull val EMPTY = Version("")

        /**
         * Separator used to separate semantic parts (major.minor.patch) of version name.
         */
        private const val SEMANTIC_SEPARATOR = "."

        /**
         * Pattern for [SEMANTIC_SEPARATOR].
         */
        private val SEMANTIC_SEPARATION_PATTERN = Pattern.compile(SEMANTIC_SEPARATOR, Pattern.LITERAL)

        /**
         * Maximum length of part (separated by [SEMANTIC_SEPARATOR]) of the semantic version name.
         */
        private const val SEMANTIC_PART_MAX_LENGTH = 4

        /**
         * Creates a new Version with the specified [name].
         *
         * @param name The desired name for the new version.
         * @return New version ready to be used.
         * @throws IllegalArgumentException If the given name is empty.
         */
        @JvmStatic fun create(@NonNull name: String): Version {
            if (name.isEmpty()) {
                throw IllegalArgumentException("Version cannot have empty name!")
            }
            return Version(name)
        }

        /**
         * Checks whether the specified [version] is actually an empty instance.
         *
         * @param version The desired version to check if it is [EMPTY] instance.
         * @return `True` if the version is empty instance, `false` otherwise, that is, it is version
         * that has been created via [create].
         */
        @JvmStatic fun isEmpty(@NonNull version: Version): Boolean = version == EMPTY

        /**
         * Compares the specified version names in a normalized fashion.
         *
         * @param firstName Name of the first version to be compared.
         * @param secondName Name of the second version to be compared.
         * @return Result of the string comparision of normalized version names.
         */
        @JvmStatic fun compare(@NonNull firstName: String, @NonNull secondName: String): Int {
            val firstNormalized = normalize(firstName)
            val secondNormalized = normalize(secondName)
            return firstNormalized.compareTo(secondNormalized)
        }

        /**
         * Performs normalization of the specified version [name].
         *
         * @param name The version name which to normalize.
         * @return Normalized name or empty string if the given name was also empty.
         */
        private fun normalize(name: String): String {
            return if (name.isNotEmpty()) normalize(name, SEMANTIC_PART_MAX_LENGTH) else ""
        }

        /**
         * Performs normalization of the specified version [name] taking into count [semanticPartMaxLength]
         *
         * @param name The version name which to normalize.
         * @param semanticPartMaxLength Maximum length of each semantic part of the name.
         * @return Normalized name.
         */
        private fun normalize(name: String, semanticPartMaxLength: Int): String {
            val versionParts = SEMANTIC_SEPARATION_PATTERN.split(name)
            val builder = StringBuilder(name.length)
            for (part in versionParts) {
                builder.append(String.format("%${semanticPartMaxLength}s", part))
            }
            return builder.toString()
        }
    }

    /*
	 * Interface ===================================================================================
	 */

    /*
     * Members =====================================================================================
     */

    /*
     * Initialization ==============================================================================
     */

    /*
     * Methods =====================================================================================
     */

    /**
     * Checks whether this version is **semantically same** as the given one.
     *
     * @param version The desired version to compare with this one.
     * @return `True` if versions are the same, that is, they have same names, `false` otherwise.
     *
     * @see isGreaterThan
     * @see isLessThan
     */
    fun isSame(@NonNull version: Version): Boolean = compareTo(version) == 0

    /**
     * Checks whether this version is **semantically greater** than the given one.
     *
     * @param version The desired version to compare with this one.
     * @return `True` if this version is greater, that is, it has greater semantic name than the
     * specified version, `false` otherwise.
     *
     * @see isLetter
     * @see isSame
     */
    fun isGreaterThan(@NonNull version: Version): Boolean = compareTo(version) > 0

    /**
     * Checks whether this version is **semantically less** than the given one.
     *
     * @param version The desired version to compare with this one.
     * @return `True` if this version is less, that is, it has lesser semantic name than the
     * specified version, `false` otherwise.
     *
     * @see isGreaterThan
     * @see isSame
     */
    fun isLessThan(@NonNull version: Version): Boolean = compareTo(version) < 0

    /*
     */
    override fun compareTo(@NonNull other: Version): Int = compare(name, other.name)

    /*
     */
    override fun equals(other: Any?): Boolean {
        if (other === this) return true
        if (other !is Version) return false
        val version = other as Version?
        return version?.name == this.name
    }

    /*
     */
    override fun hashCode(): Int = name.hashCode()

    /*
     */
    override fun toString(): String = if (this == EMPTY) "EMPTY" else "Version($name)"

    /*
     * Inner classes ===============================================================================
     */
}