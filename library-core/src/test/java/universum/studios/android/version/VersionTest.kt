/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.version

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.greaterThan
import org.hamcrest.Matchers.lessThan
import org.junit.Test
import universum.studios.android.testing.TestCase

/**
 * @author Martin Albedinsky
 */
class VersionTest : TestCase {

	@Test fun emptyInstance() {
        // Assert:
        assertThat(Version.EMPTY, `is`(notNullValue()))
        assertThat(Version.EMPTY.name, `is`(""))
        assertThat(Version.EMPTY.toString(), `is`("EMPTY"))
	}

    @Test fun create() {
        // Act + Assert:
        val version = Version.create("1.0.0")
        assertThat(version, `is`(notNullValue()))
        assertThat(version.name, `is`("1.0.0"))
    }

    @Test(expected = IllegalArgumentException::class)
    fun createWithInvalidName() {
        // Act:
        Version.create("")
    }

    @Test fun isEmpty() {
        // Act + Assert:
        assertThat(Version.isEmpty(Version.EMPTY), `is`(true))
        assertThat(Version.isEmpty(Version.create("1.0.0")), `is`(false))
    }

    @Test fun compareViaVersionNames() {
        // Act + Assert:
        assertThat(Version.compare("", ""), `is`(0))
        assertThat(Version.compare("1.0.0", "1.0.0"), `is`(0))
        assertThat(Version.compare("1.0.1", "1.0.0"), `is`(greaterThan(0)))
        assertThat(Version.compare("1.0.0", "1.0.1"), `is`(lessThan(0)))
        assertThat(Version.compare("2.0.0", "2.0.0"), `is`(0))
        assertThat(Version.compare("2.0.0-alpha1", "2.0.0"), `is`(greaterThan(0)))
    }

    @Test fun isSame() {
        // Act + Assert:
        assertThat(Version.create("1.0.0").isSame(Version.create("1.0.0")), `is`(true))
        assertThat(Version.create("1.0.0").isSame(Version.create("1.0.1")), `is`(false))
        assertThat(Version.create("1.0.0").isSame(Version.create("1.1.1")), `is`(false))
        assertThat(Version.create("1.0.0").isSame(Version.create("2.0.0")), `is`(false))
    }

    @Test fun isGreaterThan() {
        // Act + Assert:
        assertThat(Version.create("1.0.0").isGreaterThan(Version.create("1.0.0")), `is`(false))
        assertThat(Version.create("1.0.1").isGreaterThan(Version.create("1.0.0")), `is`(true))
        assertThat(Version.create("1.1.1").isGreaterThan(Version.create("1.0.0")), `is`(true))
        assertThat(Version.create("2.0.0").isGreaterThan(Version.create("1.0.0")), `is`(true))
        assertThat(Version.create("1.0.0").isGreaterThan(Version.create("1.0.1")), `is`(false))
        assertThat(Version.create("1.0.0").isGreaterThan(Version.create("1.1.1")), `is`(false))
        assertThat(Version.create("1.0.0").isGreaterThan(Version.create("2.0.0")), `is`(false))
    }

    @Test fun isLessThan() {
        // Act + Assert:
        assertThat(Version.create("1.0.0").isLessThan(Version.create("1.0.0")), `is`(false))
        assertThat(Version.create("1.0.0").isLessThan(Version.create("1.0.1")), `is`(true))
        assertThat(Version.create("1.0.0").isLessThan(Version.create("1.1.1")), `is`(true))
        assertThat(Version.create("1.0.0").isLessThan(Version.create("2.0.0")), `is`(true))
        assertThat(Version.create("1.0.1").isLessThan(Version.create("1.0.0")), `is`(false))
        assertThat(Version.create("1.1.1").isLessThan(Version.create("1.0.0")), `is`(false))
        assertThat(Version.create("2.0.0").isLessThan(Version.create("1.0.0")), `is`(false))
    }

    @Test fun compareTo() {
        // Act + Assert:
        assertThat(Version.create("1.0.0").compareTo(Version.create("1.0.0")), `is`(Version.compare("1.0.0", "1.0.0")))
        assertThat(Version.create("1.0.0").compareTo(Version.create("1.0.1")), `is`(Version.compare("1.0.0", "1.0.1")))
        assertThat(Version.create("1.0.0").compareTo(Version.create("1.1.1")), `is`(Version.compare("1.0.0", "1.1.1")))
        assertThat(Version.create("1.0.1").compareTo(Version.create("1.0.0")), `is`(Version.compare("1.0.1", "1.0.0")))
        assertThat(Version.create("1.1.1").compareTo(Version.create("1.0.0")), `is`(Version.compare("1.1.1", "1.0.0")))
    }

    @Test fun equals() {
        // Act + Assert:
        assertThat(Version.create("1.0.0") == Version.create("1.0.0"), `is`(true))
        assertThat(Version.create("1.0.0") == Version.create("1.0.1"), `is`(false))
        assertThat(Version.create("1.0.0") == Version.create("1.1.1"), `is`(false))
        assertThat(Version.create("1.0.0") == Version.create("2.0.0"), `is`(false))
        assertThat(Version.create("2.0.0") == Version.create("2.0.0"), `is`(true))
    }

    @Test fun hashCodeImplementation() {
        // Act + Assert:
        assertThat(Version.create("1.0.0").hashCode(), `is`("1.0.0".hashCode()))
        assertThat(Version.create("1.1.1").hashCode(), `is`("1.1.1".hashCode()))
    }

    @Test fun toStringImplementation() {
        // Act + Assert:
        assertThat(Version.create("1.0.0").toString(), `is`("Version(1.0.0)"))
        assertThat(Version.create("1.1.1").toString(), `is`("Version(1.1.1)"))
        assertThat(Version.create("2.0.0").toString(), `is`("Version(2.0.0)"))
    }
}