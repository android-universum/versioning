Versioning-Core
===============

This module contains core version elements.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Aversioning/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Aversioning/_latestVersion)

### Gradle ###

    compile "universum.studios.android:versioning-core:${DESIRED_VERSION}@aar"

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [Version](https://bitbucket.org/android-universum/versioning/src/main/library-core/src/main/java/universum/studios/android/version/Version.kt)
