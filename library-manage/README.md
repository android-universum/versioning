Versioning-Manage
===============

This module contains elements which simplify application versions management.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Aversioning/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Aversioning/_latestVersion)

### Gradle ###

    compile "universum.studios.android:versioning-manage:${DESIRED_VERSION}@aar"

_depends on:_
[versioning-core](https://bitbucket.org/android-universum/versioning/src/main/library-core)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [VersionManager](https://bitbucket.org/android-universum/versioning/src/main/library-manage/src/main/java/universum/studios/android/version/VersionManager.kt)
- [VersionRepository](https://bitbucket.org/android-universum/versioning/src/main/library-manage/src/main/java/universum/studios/android/version/VersionRepository.kt)
