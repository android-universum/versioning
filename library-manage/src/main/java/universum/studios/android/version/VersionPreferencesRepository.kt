/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.version

import android.content.SharedPreferences
import androidx.annotation.NonNull
import androidx.annotation.VisibleForTesting

/**
 * A [VersionRepository] implementation which uses [SharedPreferences] to persist application versions.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @constructor Creates a new instance of VersionPreferencesRepository with the specified `preferences`.
 * @param preferences The desired shared preferences where should be persisted application versions
 * by the new repository.
 */
class VersionPreferencesRepository(
        /**
         * Shared preferences
         */
        @NonNull private val preferences: SharedPreferences) : VersionRepository {

    /*
     * Companion ===================================================================================
     */

    /**
     */
    companion object {

        /**
         * Key under which is persisted application's actual version in preferences.
         */
        @VisibleForTesting internal const val KEY_VERSION_ACTUAL = "App.VERSION.Actual"

        /**
         * Key under which is persisted application's previous version in preferences.
         */
        @VisibleForTesting internal const val KEY_VERSION_PREVIOUS = "App.VERSION.Previous"
    }

    /*
	 * Interface ===================================================================================
	 */

    /*
     * Members =====================================================================================
     */

    /*
     * Initialization ==============================================================================
     */

    /*
     * Methods =====================================================================================
     */

    /*
     */
    @NonNull override fun getActualVersion(): Version = retrieveVersionForKey(KEY_VERSION_ACTUAL)

    /*
     */
    override fun persistActualVersion(@NonNull version: Version) {
        this.persistVersionForKey(KEY_VERSION_ACTUAL, version)
    }

    /*
     */
    @NonNull override fun getPreviousVersion(): Version = retrieveVersionForKey(KEY_VERSION_PREVIOUS)

    /*
     */
    override fun persistPreviousVersion(@NonNull version: Version) {
        this.persistVersionForKey(KEY_VERSION_PREVIOUS, version)
    }

    /**
     * Retrieves a version which is persisted under the specified [versionKey].
     *
     * @param versionKey Key of the desired version to retrieve.
     * @return Version retrieved from preferences or [Version.EMPTY] if there is no such version
     * persisted for the specified key.
     */
    private fun retrieveVersionForKey(versionKey: String): Version {
        val versionName = preferences.getString(versionKey, null)
        return if (versionName.isNullOrEmpty()) Version.EMPTY else Version.create(versionName)
    }

    /**
     * Persists the given [version] in preferences under the specified [versionKey].
     *
     * @param versionKey Key of the desired version to persist.
     * @param version The desired version to by persisted.
     */
    private fun persistVersionForKey(versionKey: String, version: Version) {
        this.preferences.edit().putString(versionKey, version.name).apply()
    }

    /*
     */
    override fun isEmpty(): Boolean = !(preferences.contains(KEY_VERSION_ACTUAL) || preferences.contains(KEY_VERSION_PREVIOUS))

    /*
     * Inner classes ===============================================================================
     */
}