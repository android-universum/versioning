/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.version

import androidx.annotation.NonNull

/**
 * Interface for storing and accessing of application actual and previous versions.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
interface VersionRepository {

    /**
     * Retrieves the application's actual version persisted via this repository (if any).
     *
     * @return Actual application version or [Version.EMPTY] if this repository does not contain any
     * such version.
     *
     * @see persistActualVersion
     * @see getPreviousVersion
     */
    @NonNull fun getActualVersion(): Version

    /**
     * Persists the given [version] as application's actual version.
     *
     * @param version The desired version which to persist as the actual one.
     *
     * @see getActualVersion
     */
    fun persistActualVersion(@NonNull version: Version)

    /**
     * Retrieves the application's previous version persisted via this repository (if any).
     *
     * @return Previous application version or [Version.EMPTY] if this repository does not contain any
     * such version.
     *
     * @see persistPreviousVersion
     * @see getActualVersion
     */
    @NonNull fun getPreviousVersion(): Version

    /**
     * Persists the given [version] as application's previous version.
     *
     * @param version The desired version which to persist as the previous one.
     *
     * @see getPreviousVersion
     */
    fun persistPreviousVersion(@NonNull version: Version)

    /**
     * Checks whether this repository contains some
     *
     * @return `True` if this repository does not contain neither actual application version nor
     * previous one, `false` if at least one of those versions is persisted and may be retrieved
     * via this repository.
     *
     * @see getActualVersion
     * @see getPreviousVersion
     */
    fun isEmpty(): Boolean
}