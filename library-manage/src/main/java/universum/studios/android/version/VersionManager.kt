/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.version

import androidx.annotation.NonNull

/**
 * Simple manager which may be used to track application updates through [Version] changes. Each
 * instance of VersionManager is required to be created with [versions repository][VersionRepository]
 * which is used to persist and retrieve persisted application versions and **actual** application
 * [version][Version] which is used to resolve whether application update was performed.
 *
 * Once created, available **check** methods of the manager may be used in order to find out whether
 * application was updated with unspecified versions transition, updated from a particular version,
 * updated to a particular version, updated from -> to a particular version. Below are listed all
 * available check methods:
 * - [checkUpdated][checkUpdated]
 * - [checkUpdatedFrom(Version)][checkUpdatedFrom]
 * - [checkUpdatedFromBelow(Version)][checkUpdatedFromBelow]
 * - [checkUpdatedTo(Version)][checkUpdatedTo]
 * - [checkUpdatedFromTo(Version, Version)][checkUpdatedFromTo]
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @constructor Creates a new instance of VersionManager with the specified `repository`.
 * @param repository The desired repository where should the new manager persist application versions.
 * @param actualVersion The actual application version.
 */
class VersionManager(
        /**
         * Repository used to persist application versions.
         */
        @NonNull repository: VersionRepository,
        /**
         * Actual application version.
         */
        @NonNull val actualVersion: Version) {

    /*
     * Companion ===================================================================================
     */

    /*
     * Interface ===================================================================================
     */

    /*
     * Members =====================================================================================
     */

    /**
     * Previous application version (if persisted). If not persisted, this will be [Version.EMPTY].
     *
     * @return The previous application version persisted in versions repository or [Version.EMPTY]
     * if there is no such version persisted yet.
     */
    @NonNull val previousVersion: Version

    /*
     * Initialization ==============================================================================
     */

    /**
     */
    init {
        val persistedActualVersion = repository.getActualVersion()
        if (Version.isEmpty(persistedActualVersion)) {
            // Application has been started with clean slate.
            this.previousVersion = Version.EMPTY
            repository.persistActualVersion(actualVersion)
        } else {
            if (actualVersion == persistedActualVersion) {
                // Application has been started in actual version.
                this.previousVersion = repository.getPreviousVersion()
            } else {
                // Application version has been updated.
                this.previousVersion = persistedActualVersion
                repository.persistPreviousVersion(persistedActualVersion)
                repository.persistActualVersion(actualVersion)
            }
        }
    }

    /*
     * Methods =====================================================================================
     */

    /**
     * Checks whether the application was updated from the specified [version] during the last update.
     *
     * This actually checks whether the specified version is identical with the previous one.
     *
     * **Note:**
     * Subsequent calls to this method will always return the same result regardless any changes
     * made to the version repository outside of this manager's context.
     *
     * @param version The desired version to check if application was updated from.
     * @return `True` if application was updated from the specified version, `false` otherwise.
     *
     * @see checkUpdated
     * @see checkUpdatedFromBelow
     * @see checkUpdatedTo
     */
    fun checkUpdatedFrom(@NonNull version: Version): Boolean = checkUpdated() && version == previousVersion

    /**
     * Checks whether the application was updated from **any** version below the specified [version]
     * (excluding) anytime during its lifecycle.
     *
     * This actually checks whether the previous version is **less than** the specified version.
     *
     * **Note:**
     * Subsequent calls to this method will always return the same result regardless any changes
     * made to the version repository outside of this manager's context.
     *
     * @param version The desired version to check if application was updated from.
     * @return `True` if application was updated from the specified version, `false` otherwise.
     *
     * @see checkUpdated
     * @see checkUpdatedTo
     */
    fun checkUpdatedFromBelow(@NonNull version: Version): Boolean = checkUpdated() && previousVersion.isLessThan(version)

    /**
     * Checks whether the application was updated to the specified [version] during the last update.
     *
     * This actually checks whether the specified version is identical with the actual one.
     *
     * **Note:**
     * Subsequent calls to this method will always return the same result regardless any changes
     * made to the version repository outside of this manager's context.
     *
     * @param version The desired version to check if application was updated to.
     * @return `True` if application was updated to the specified version, `false` otherwise.
     *
     * @see checkUpdated
     * @see checkUpdatedFrom
     */
    fun checkUpdatedTo(@NonNull version: Version): Boolean = checkUpdated() && version == actualVersion

    /**
     * Checks whether the application was updated [from] the specified version [to] the specified
     * version during the last update.
     *
     * **Results:**
     * - `TRUE` -> means that application was updated and **previous persisted** version is **identical**
     * with the specified from version and **actual** version is **identical** with specified to version.
     * - `FALSE` -> means that the application was never updated, or it was updated, but either from
     * a different or to a different version or both.
     *
     * **Note:**
     * Subsequent calls to this method will always return the same result regardless any changes
     * made to the version repository outside of this manager's context.
     *
     * @param from The desired version to check if application was updated from.
     * @param to The desired version to check if application was updated to.
     * @return `True` if application was updated from and to the specified version, `false` otherwise.
     *
     * @see checkUpdated
     * @see checkUpdatedFrom
     * @see checkUpdatedTo
     */
    fun checkUpdatedFromTo(@NonNull from: Version, @NonNull to: Version): Boolean = checkUpdated() && from == previousVersion && to == actualVersion

    /**
     * Checks whether the application was updated from an unspecified previous version to an
     * unspecified new version anytime during its lifecycle.
     *
     * **Results:**
     * - `TRUE` -> means that there was already **persisted previous** application version which is
     * **different** from the actual one.
     * - `FALSE` -> means that the application is still in its first version, that is, it was
     * **never updated** yet.
     *
     * **Note:**
     * Subsequent calls to this method will always return the same result regardless any changes
     * made to the version repository outside of this manager's context.
     *
     * @return `True` if application was updated, `false` otherwise.
     */
    fun checkUpdated(): Boolean = !Version.isEmpty(previousVersion) && actualVersion != previousVersion

    /*
     * Inner classes ===============================================================================
     */
}