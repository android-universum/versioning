/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.version

import android.content.SharedPreferences
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test
import org.mockito.ArgumentMatchers.eq
import org.mockito.Mockito.`when`
import org.mockito.Mockito.anyString
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.Mockito.verifyNoInteractions
import org.mockito.Mockito.verifyNoMoreInteractions
import universum.studios.android.testing.TestCase

/**
 * @author Martin Albedinsky
 */
class VersionPreferencesRepositoryTest : TestCase {

    @Test fun instantiation() {
        // Arrange:
        val mockPreferences = mock(SharedPreferences::class.java)
        // Act:
        VersionPreferencesRepository(mockPreferences)
        // Assert:
        verifyNoInteractions(mockPreferences)
    }

    @Test fun getActualVersionPersisted() {
        // Arrange:
        val mockPreferences = mock(SharedPreferences::class.java)
        `when`(mockPreferences.getString(VersionPreferencesRepository.KEY_VERSION_ACTUAL, null)).thenReturn("2.0.0")
        val repository = VersionPreferencesRepository(mockPreferences)
        // Act:
        val version = repository.getActualVersion()
        // Assert:
        assertThat(version, `is`(notNullValue()))
        assertThat(version.name, `is`("2.0.0"))
        verify(mockPreferences).getString(VersionPreferencesRepository.KEY_VERSION_ACTUAL, null)
        verifyNoMoreInteractions(mockPreferences)
    }

    @Test fun getActualVersionNotPersisted() {
        // Arrange:
        val mockPreferences = mock(SharedPreferences::class.java)
        `when`(mockPreferences.getString(VersionPreferencesRepository.KEY_VERSION_ACTUAL, null)).thenReturn(null)
        val repository = VersionPreferencesRepository(mockPreferences)
        // Act:
        val version = repository.getActualVersion()
        // Assert:
        assertThat(version, `is`(Version.EMPTY))
        verify(mockPreferences).getString(VersionPreferencesRepository.KEY_VERSION_ACTUAL, null)
        verifyNoMoreInteractions(mockPreferences)
    }

    @Test fun getActualVersionPersistedWithEmptyName() {
        // Arrange:
        val mockPreferences = mock(SharedPreferences::class.java)
        `when`(mockPreferences.getString(VersionPreferencesRepository.KEY_VERSION_ACTUAL, null)).thenReturn("")
        val repository = VersionPreferencesRepository(mockPreferences)
        // Act:
        val version = repository.getActualVersion()
        // Assert:
        assertThat(version, `is`(Version.EMPTY))
        verify(mockPreferences).getString(VersionPreferencesRepository.KEY_VERSION_ACTUAL, null)
        verifyNoMoreInteractions(mockPreferences)
    }

    @Test fun persistActualVersion() {
        // Arrange:
        val mockPreferences = mock(SharedPreferences::class.java)
        val mockEditor = mock(SharedPreferences.Editor::class.java)
        `when`(mockEditor.putString(eq(VersionPreferencesRepository.KEY_VERSION_ACTUAL), anyString())).thenReturn(mockEditor)
        `when`(mockPreferences.edit()).thenReturn(mockEditor)
        val repository = VersionPreferencesRepository(mockPreferences)
        val version = Version.create("2.0.0")
        // Act:
        repository.persistActualVersion(version)
        // Assert:
        verify(mockEditor).putString(eq(VersionPreferencesRepository.KEY_VERSION_ACTUAL), eq(version.name))
        verify(mockEditor).apply()
        verifyNoMoreInteractions(mockEditor)
    }

    @Test fun getPreviousVersionPersisted() {
        // Arrange:
        val mockPreferences = mock(SharedPreferences::class.java)
        `when`(mockPreferences.getString(VersionPreferencesRepository.KEY_VERSION_PREVIOUS, null)).thenReturn("1.0.0")
        val repository = VersionPreferencesRepository(mockPreferences)
        // Act:
        val version = repository.getPreviousVersion()
        // Assert:
        assertThat(version, `is`(notNullValue()))
        assertThat(version.name, `is`("1.0.0"))
        verify(mockPreferences).getString(VersionPreferencesRepository.KEY_VERSION_PREVIOUS, null)
        verifyNoMoreInteractions(mockPreferences)
    }

    @Test fun getPreviousVersionNotPersisted() {
        // Arrange:
        val mockPreferences = mock(SharedPreferences::class.java)
        `when`(mockPreferences.getString(VersionPreferencesRepository.KEY_VERSION_PREVIOUS, null)).thenReturn(null)
        val repository = VersionPreferencesRepository(mockPreferences)
        // Act:
        val version = repository.getPreviousVersion()
        // Assert:
        assertThat(version, `is`(Version.EMPTY))
        verify(mockPreferences).getString(VersionPreferencesRepository.KEY_VERSION_PREVIOUS, null)
        verifyNoMoreInteractions(mockPreferences)
    }

    @Test fun getPreviousVersionPersistedWithEmptyName() {
        // Arrange:
        val mockPreferences = mock(SharedPreferences::class.java)
        `when`(mockPreferences.getString(VersionPreferencesRepository.KEY_VERSION_PREVIOUS, null)).thenReturn("")
        val repository = VersionPreferencesRepository(mockPreferences)
        // Act:
        val version = repository.getPreviousVersion()
        // Assert:
        assertThat(version, `is`(Version.EMPTY))
        verify(mockPreferences).getString(VersionPreferencesRepository.KEY_VERSION_PREVIOUS, null)
        verifyNoMoreInteractions(mockPreferences)
    }

    @Test fun persistPreviousVersion() {
        // Arrange:
        val mockPreferences = mock(SharedPreferences::class.java)
        val mockEditor = mock(SharedPreferences.Editor::class.java)
        `when`(mockEditor.putString(eq(VersionPreferencesRepository.KEY_VERSION_PREVIOUS), anyString())).thenReturn(mockEditor)
        `when`(mockPreferences.edit()).thenReturn(mockEditor)
        val repository = VersionPreferencesRepository(mockPreferences)
        val version = Version.create("1.0.0")
        // Act:
        repository.persistPreviousVersion(version)
        // Assert:
        verify(mockEditor).putString(eq(VersionPreferencesRepository.KEY_VERSION_PREVIOUS), eq(version.name))
        verify(mockEditor).apply()
        verifyNoMoreInteractions(mockEditor)
    }

    @Test fun isEmptyWhenEmpty() {
        // Arrange:
        val mockPreferences = mock(SharedPreferences::class.java)
        `when`(mockPreferences.contains(VersionPreferencesRepository.KEY_VERSION_ACTUAL)).thenReturn(false)
        `when`(mockPreferences.contains(VersionPreferencesRepository.KEY_VERSION_PREVIOUS)).thenReturn(false)
        val repository = VersionPreferencesRepository(mockPreferences)
        // Act + Assert::
        assertThat(repository.isEmpty(), `is`(true))
        verify(mockPreferences).contains(VersionPreferencesRepository.KEY_VERSION_ACTUAL)
        verify(mockPreferences).contains(VersionPreferencesRepository.KEY_VERSION_PREVIOUS)
        verifyNoMoreInteractions(mockPreferences)
    }

    @Test fun isEmptyWithActualVersion() {
        // Arrange:
        val mockPreferences = mock(SharedPreferences::class.java)
        `when`(mockPreferences.contains(VersionPreferencesRepository.KEY_VERSION_ACTUAL)).thenReturn(true)
        `when`(mockPreferences.contains(VersionPreferencesRepository.KEY_VERSION_PREVIOUS)).thenReturn(false)
        val repository = VersionPreferencesRepository(mockPreferences)
        // Act + Assert:
        assertThat(repository.isEmpty(), `is`(false))
        verify(mockPreferences).contains(VersionPreferencesRepository.KEY_VERSION_ACTUAL)
        verifyNoMoreInteractions(mockPreferences)
    }

    @Test fun isEmptyWithPreviousVersion() {
        // Arrange:
        val mockPreferences = mock(SharedPreferences::class.java)
        `when`(mockPreferences.contains(VersionPreferencesRepository.KEY_VERSION_ACTUAL)).thenReturn(false)
        `when`(mockPreferences.contains(VersionPreferencesRepository.KEY_VERSION_PREVIOUS)).thenReturn(true)
        val repository = VersionPreferencesRepository(mockPreferences)
        // Act + Assert:
        assertThat(repository.isEmpty(), `is`(false))
        verify(mockPreferences).contains(VersionPreferencesRepository.KEY_VERSION_ACTUAL)
        verify(mockPreferences).contains(VersionPreferencesRepository.KEY_VERSION_PREVIOUS)
        verifyNoMoreInteractions(mockPreferences)
    }

    @Test fun isEmptyWithBothVersions() {
        // Arrange:
        val mockPreferences = mock(SharedPreferences::class.java)
        `when`(mockPreferences.contains(VersionPreferencesRepository.KEY_VERSION_ACTUAL)).thenReturn(true)
        `when`(mockPreferences.contains(VersionPreferencesRepository.KEY_VERSION_PREVIOUS)).thenReturn(true)
        val repository = VersionPreferencesRepository(mockPreferences)
        // Act + Assert:
        assertThat(repository.isEmpty(), `is`(false))
        verify(mockPreferences).contains(VersionPreferencesRepository.KEY_VERSION_ACTUAL)
        verifyNoMoreInteractions(mockPreferences)
    }
}