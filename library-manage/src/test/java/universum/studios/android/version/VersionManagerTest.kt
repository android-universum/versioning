/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.version

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.Mockito.verifyNoMoreInteractions
import universum.studios.android.testing.TestCase

/**
 * @author Martin Albedinsky
 */
class VersionManagerTest : TestCase {

    companion object {

        private val VERSION_100 = Version.create("1.0.0")
        private val VERSION_101 = Version.create("1.0.1")
        private val VERSION_200 = Version.create("2.0.0")
        private val VERSION_210 = Version.create("2.1.0")
        private val VERSION_300 = Version.create("3.0.0")
        private val VERSION_400 = Version.create("4.0.0")
    }

    @Test fun instantiationAtAppCleanStart() {
        // Arrange:
        val mockRepository = createMockRepositoryWithVersions(Version.EMPTY, Version.EMPTY)
        // Act:
        val manager = VersionManager(mockRepository, VERSION_300)
        // Assert:
        assertThat(manager.actualVersion, `is`(VERSION_300))
        verify(mockRepository).getActualVersion()
        verify(mockRepository).persistActualVersion(VERSION_300)
        verifyNoMoreInteractions(mockRepository)
    }

    @Test fun instantiationAtAppSubsequentStart() {
        // Arrange:
        val mockRepository = createMockRepositoryWithVersions(VERSION_300, Version.EMPTY)
        // Act:
        val manager = VersionManager(mockRepository, VERSION_300)
        // Assert:
        assertThat(manager.actualVersion, `is`(VERSION_300))
        assertThat(manager.previousVersion, `is`(Version.EMPTY))
        verify(mockRepository).getActualVersion()
        verify(mockRepository).getPreviousVersion()
        verifyNoMoreInteractions(mockRepository)
    }

    @Test fun instantiationAtAppSubsequentStartDuringUpdate() {
        // Arrange:
        val mockRepository = createMockRepositoryWithVersions(VERSION_200, VERSION_100)
        // Act:
        val manager = VersionManager(mockRepository, VERSION_300)
        // Assert:
        assertThat(manager.actualVersion, `is`(VERSION_300))
        assertThat(manager.previousVersion, `is`(VERSION_200))
        verify(mockRepository).getActualVersion()
        verify(mockRepository).persistActualVersion(VERSION_300)
        verify(mockRepository).persistPreviousVersion(VERSION_200)
        verifyNoMoreInteractions(mockRepository)
    }

    @Test fun instantiationAtAppSubsequentStartAfterUpdate() {
        // Arrange:
        val mockRepository = createMockRepositoryWithVersions(VERSION_300, VERSION_200)
        // Act:
        val manager = VersionManager(mockRepository, VERSION_300)
        // Assert:
        assertThat(manager.actualVersion, `is`(VERSION_300))
        assertThat(manager.previousVersion, `is`(VERSION_200))
        verify(mockRepository).getActualVersion()
        verify(mockRepository).getPreviousVersion()
        verifyNoMoreInteractions(mockRepository)
    }

    @Test fun checkUpdatedFromAtAppFirstStart() {
        assertThatCheckUpdatedFromReturnsWithResult(
                // Actual version:
                VERSION_100,
                // Persisted actual & previous version:
                Version.EMPTY, Version.EMPTY,
                // From version:
                VERSION_100,
                // Check result:
                false
        )
        assertThatCheckUpdatedFromReturnsWithResult(
                // Actual version:
                VERSION_100,
                // Persisted actual & previous version:
                Version.EMPTY, Version.EMPTY,
                // From version:
                VERSION_200,
                // Check result:
                false
        )
    }

    @Test fun checkUpdatedFromAtAppSubsequentStart() {
        assertThatCheckUpdatedFromReturnsWithResult(
                // Actual version:
                VERSION_100,
                // Persisted actual & previous version:
                VERSION_100, Version.EMPTY,
                // From version:
                VERSION_100,
                // Check result:
                false
        )
        assertThatCheckUpdatedFromReturnsWithResult(
                // Actual version:
                VERSION_100,
                // Persisted actual & previous version:
                VERSION_100, Version.EMPTY,
                // From version:
                VERSION_200,
                // Check result:
                false
        )
    }

    @Test fun checkUpdatedFromAtAppSubsequentStartDuringFirstUpdate() {
        assertThatCheckUpdatedFromReturnsWithResult(
                // Actual version:
                VERSION_200,
                // Persisted actual & previous version:
                VERSION_100, Version.EMPTY,
                // From version:
                VERSION_100,
                // Check result:
                true
        )
        assertThatCheckUpdatedFromReturnsWithResult(
                // Actual version:
                VERSION_200,
                // Persisted actual & previous version:
                VERSION_100, Version.EMPTY,
                // From version:
                VERSION_200,
                // Check result:
                false
        )
        assertThatCheckUpdatedFromReturnsWithResult(
                // Actual version:
                VERSION_200,
                // Persisted actual & previous version:
                VERSION_100, Version.EMPTY,
                // From version:
                VERSION_300,
                // Check result:
                false
        )
    }

    @Test fun checkUpdatedFromAtAppSubsequentStartAfterFirstUpdate() {
        assertThatCheckUpdatedFromReturnsWithResult(
                // Actual version:
                VERSION_200,
                // Persisted actual & previous version:
                VERSION_200, VERSION_100,
                // From version:
                VERSION_100,
                // Check result:
                true
        )
        assertThatCheckUpdatedFromReturnsWithResult(
                // Actual version:
                VERSION_200,
                // Persisted actual & previous version:
                VERSION_200, VERSION_100,
                // From version:
                VERSION_200,
                // Check result:
                false
        )
        assertThatCheckUpdatedFromReturnsWithResult(
                // Actual version:
                VERSION_200,
                // Persisted actual & previous version:
                VERSION_200, VERSION_100,
                // From version:
                VERSION_300,
                // Check result:
                false
        )
    }

    @Test fun checkUpdatedFromAtAppSubsequentStartDuringSubsequentUpdate() {
        assertThatCheckUpdatedFromReturnsWithResult(
                // Actual version:
                VERSION_300,
                // Persisted actual & previous version:
                VERSION_200, VERSION_100,
                // From version:
                VERSION_200,
                // Check result:
                true
        )
        assertThatCheckUpdatedFromReturnsWithResult(
                // Actual version:
                VERSION_300,
                // Persisted actual & previous version:
                VERSION_200, VERSION_100,
                // From version:
                VERSION_100,
                // Check result:
                false
        )
        assertThatCheckUpdatedFromReturnsWithResult(
                // Actual version:
                VERSION_300,
                // Persisted actual & previous version:
                VERSION_200, VERSION_100,
                // From version:
                VERSION_300,
                // Check result:
                false
        )
    }

    @Test fun checkUpdatedFromAtAppSubsequentStartAfterSubsequentUpdate() {
        assertThatCheckUpdatedFromReturnsWithResult(
                // Actual version:
                VERSION_300,
                // Persisted actual & previous version:
                VERSION_300, VERSION_200,
                // From version:
                VERSION_200,
                // Check result:
                true
        )
        assertThatCheckUpdatedFromReturnsWithResult(
                // Actual version:
                VERSION_300,
                // Persisted actual & previous version:
                VERSION_300, VERSION_200,
                // From version:
                VERSION_100,
                // Check result:
                false
        )
        assertThatCheckUpdatedFromReturnsWithResult(
                // Actual version:
                VERSION_300,
                // Persisted actual & previous version:
                VERSION_300, VERSION_200,
                // From version:
                VERSION_300,
                // Check result:
                false
        )
    }

    private fun assertThatCheckUpdatedFromReturnsWithResult(
            actualVersion: Version,
            persistedVersionActual: Version,
            persistedVersionPrevious: Version,
            versionFrom: Version,
            result: Boolean
    ) {
        // Arrange:
        val mockRepository = createMockRepositoryWithVersions(persistedVersionActual, persistedVersionPrevious)
        val manager = VersionManager(mockRepository, actualVersion)
        // Act + Assert:
        assertThat(manager.checkUpdatedFrom(versionFrom),`is`(result))
    }

    @Test fun checkUpdatedFromBelowAtAppFirstStart() {
        assertThatCheckUpdatedFromBelowReturnsWithResult(
                // Actual version:
                VERSION_100,
                // Persisted actual & previous version:
                Version.EMPTY, Version.EMPTY,
                // From version below:
                VERSION_100,
                // Check result:
                false
        )
        assertThatCheckUpdatedFromBelowReturnsWithResult(
                // Actual version:
                VERSION_100,
                // Persisted actual & previous version:
                Version.EMPTY, Version.EMPTY,
                // From version below:
                VERSION_200,
                // Check result:
                false
        )
    }

    @Test fun checkUpdatedFromBelowAtAppSubsequentStart() {
        assertThatCheckUpdatedFromBelowReturnsWithResult(
                // Actual version:
                VERSION_100,
                // Persisted actual & previous version:
                VERSION_100, Version.EMPTY,
                // From version below:
                VERSION_100,
                // Check result:
                false
        )
        assertThatCheckUpdatedFromBelowReturnsWithResult(
                // Actual version:
                VERSION_100,
                // Persisted actual & previous version:
                VERSION_100, Version.EMPTY,
                // From version below:
                VERSION_200,
                // Check result:
                false
        )
    }

    @Test fun checkUpdatedFromBelowAtAppSubsequentStartDuringFirstUpdate() {
        assertThatCheckUpdatedFromBelowReturnsWithResult(
                // Actual version:
                VERSION_200,
                // Persisted actual & previous version:
                VERSION_100, Version.EMPTY,
                // From version below:
                VERSION_101,
                // Check result:
                true
        )
        assertThatCheckUpdatedFromBelowReturnsWithResult(
                // Actual version:
                VERSION_200,
                // Persisted actual & previous version:
                VERSION_100, Version.EMPTY,
                // From version below:
                VERSION_200,
                // Check result:
                true
        )
        assertThatCheckUpdatedFromBelowReturnsWithResult(
                // Actual version:
                VERSION_200,
                // Persisted actual & previous version:
                VERSION_100, Version.EMPTY,
                // From version below:
                VERSION_100,
                // Check result:
                false
        )
    }

    @Test fun checkUpdatedFromBelowAtAppSubsequentStartAfterFirstUpdate() {
        assertThatCheckUpdatedFromBelowReturnsWithResult(
                // Actual version:
                VERSION_200,
                // Persisted actual & previous version:
                VERSION_101, VERSION_100,
                // From version below:
                VERSION_200,
                // Check result:
                true
        )
        assertThatCheckUpdatedFromBelowReturnsWithResult(
                // Actual version:
                VERSION_200,
                // Persisted actual & previous version:
                VERSION_101, VERSION_100,
                // From version below:
                VERSION_101,
                // Check result:
                false
        )
        assertThatCheckUpdatedFromBelowReturnsWithResult(
                // Actual version:
                VERSION_200,
                // Persisted actual & previous version:
                VERSION_101, VERSION_100,
                // From version below:
                VERSION_100,
                // Check result:
                false
        )
    }

    @Test fun checkUpdatedFromBelowAtAppSubsequentStartDuringSubsequentUpdate() {
        assertThatCheckUpdatedFromBelowReturnsWithResult(
                // Actual version:
                VERSION_210,
                // Persisted actual & previous version:
                VERSION_200, VERSION_101,
                // From version below:
                VERSION_210,
                // Check result:
                true
        )
        assertThatCheckUpdatedFromBelowReturnsWithResult(
                // Actual version:
                VERSION_210,
                // Persisted actual & previous version:
                VERSION_200, VERSION_101,
                // From version below:
                VERSION_200,
                // Check result:
                false
        )
    }

    @Test fun checkUpdatedFromBelowAtAppSubsequentStartAfterSubsequentUpdate() {
        assertThatCheckUpdatedFromBelowReturnsWithResult(
                // Actual version:
                VERSION_210,
                // Persisted actual & previous version:
                VERSION_210, VERSION_200,
                // From version below:
                VERSION_300,
                // Check result:
                true
        )
        assertThatCheckUpdatedFromBelowReturnsWithResult(
                // Actual version:
                VERSION_210,
                // Persisted actual & previous version:
                VERSION_210, VERSION_200,
                // From version below:
                VERSION_200,
                // Check result:
                false
        )
        assertThatCheckUpdatedFromBelowReturnsWithResult(
                // Actual version:
                VERSION_210,
                // Persisted actual & previous version:
                VERSION_210, VERSION_200,
                // From version below:
                VERSION_101,
                // Check result:
                false
        )
    }

    private fun assertThatCheckUpdatedFromBelowReturnsWithResult(
            actualVersion: Version,
            persistedVersionActual: Version,
            persistedVersionPrevious: Version,
            versionFrom: Version,
            result: Boolean
    ) {
        // Arrange:
        val mockRepository = createMockRepositoryWithVersions(persistedVersionActual, persistedVersionPrevious)
        val manager = VersionManager(mockRepository, actualVersion)
        // Act + Assert:
        assertThat(manager.checkUpdatedFromBelow(versionFrom),`is`(result))
    }

    @Test fun checkUpdatedToAtAppFirstStart() {
        assertThatCheckUpdatedToReturnsWithResult(
                // Actual version:
                VERSION_100,
                // Persisted actual & previous version:
                Version.EMPTY, Version.EMPTY,
                // To version:
                VERSION_100,
                // Check result:
                false
        )
    }

    @Test fun checkUpdatedToAtAppSubsequentStart() {
        assertThatCheckUpdatedToReturnsWithResult(
                // Actual version:
                VERSION_100,
                // Persisted actual & previous version:
                VERSION_100, Version.EMPTY,
                // To version:
                VERSION_100,
                // Check result:
                false
        )
    }

    @Test fun checkUpdatedToAtAppSubsequentStartDuringFirstUpdate() {
        assertThatCheckUpdatedToReturnsWithResult(
                // Actual version:
                VERSION_200,
                // Persisted actual & previous version:
                VERSION_100, Version.EMPTY,
                // To version:
                VERSION_200,
                // Check result:
                true
        )
        assertThatCheckUpdatedToReturnsWithResult(
                // Actual version:
                VERSION_200,
                // Persisted actual & previous version:
                VERSION_100, Version.EMPTY,
                // To version:
                VERSION_300,
                // Check result:
                false
        )
    }

    @Test fun checkUpdatedToAtAppSubsequentStartAfterFirstUpdate() {
        assertThatCheckUpdatedToReturnsWithResult(
                // Actual version:
                VERSION_200,
                // Persisted actual & previous version:
                VERSION_200, VERSION_100,
                // To version:
                VERSION_200,
                // Check result:
                true
        )
        assertThatCheckUpdatedToReturnsWithResult(
                // Actual version:
                VERSION_200,
                // Persisted actual & previous version:
                VERSION_200, VERSION_100,
                // To version:
                VERSION_300,
                // Check result:
                false
        )
    }

    @Test fun checkUpdatedToAtAppSubsequentStartDuringSubsequentUpdate() {
        assertThatCheckUpdatedToReturnsWithResult(
                // Actual version:
                VERSION_300,
                // Persisted actual & previous version:
                VERSION_200, VERSION_100,
                // To version:
                VERSION_300,
                // Check result:
                true
        )
        assertThatCheckUpdatedToReturnsWithResult(
                // Actual version:
                VERSION_300,
                // Persisted actual & previous version:
                VERSION_200, VERSION_100,
                // To version:
                VERSION_400,
                // Check result:
                false
        )
    }

    @Test fun checkUpdatedToAtAppSubsequentStartAfterSubsequentUpdate() {
        assertThatCheckUpdatedToReturnsWithResult(
                // Actual version:
                VERSION_300,
                // Persisted actual & previous version:
                VERSION_300, VERSION_200,
                // To version:
                VERSION_300,
                // Check result:
                true
        )
        assertThatCheckUpdatedToReturnsWithResult(
                // Actual version:
                VERSION_300,
                // Persisted actual & previous version:
                VERSION_300, VERSION_200,
                // To version:
                VERSION_400,
                // Check result:
                false
        )
    }

    private fun assertThatCheckUpdatedToReturnsWithResult(
            actualVersion: Version,
            persistedVersionActual: Version,
            persistedVersionPrevious: Version,
            versionTo: Version,
            result: Boolean
    ) {
        // Arrange:
        val mockRepository = createMockRepositoryWithVersions(persistedVersionActual, persistedVersionPrevious)
        val manager = VersionManager(mockRepository, actualVersion)
        // Act + Assert:
        assertThat(manager.checkUpdatedTo(versionTo),`is`(result))
    }

    @Test fun checkUpdatedFromToAtAppFirstStart() {
        assertThatCheckUpdatedFromToReturnsWithResult(
                // Actual version:
                VERSION_100,
                // Persisted actual & previous version:
                Version.EMPTY, Version.EMPTY,
                // From & to version:
                VERSION_100, VERSION_200,
                // Check result:
                false
        )
    }

    @Test fun checkUpdatedFromToAtAppSubsequentStart() {
        assertThatCheckUpdatedFromToReturnsWithResult(
                // Actual version:
                VERSION_100,
                // Persisted actual & previous version:
                VERSION_100, Version.EMPTY,
                // From & to version:
                VERSION_100, VERSION_200,
                // Check result:
                false
        )
    }

    @Test fun checkUpdatedFromToAtAppSubsequentStartDuringFirstUpdate() {
        assertThatCheckUpdatedFromToReturnsWithResult(
                // Actual version:
                VERSION_200,
                // Persisted actual & previous version:
                VERSION_100, Version.EMPTY,
                // From & to version:
                VERSION_100, VERSION_200,
                // Check result:
                true
        )
        assertThatCheckUpdatedFromToReturnsWithResult(
                // Actual version:
                VERSION_200,
                // Persisted actual & previous version:
                VERSION_100, Version.EMPTY,
                // From & to version:
                VERSION_100, VERSION_300,
                // Check result:
                false
        )
    }

    @Test fun checkUpdatedFromToAtAppSubsequentStartAfterFirstUpdate() {
        assertThatCheckUpdatedFromToReturnsWithResult(
                // Actual version:
                VERSION_200,
                // Persisted actual & previous version:
                VERSION_200, VERSION_100,
                // From & to version:
                VERSION_100, VERSION_200,
                // Check result:
                true
        )
        assertThatCheckUpdatedFromToReturnsWithResult(
                // Actual version:
                VERSION_200,
                // Persisted actual & previous version:
                VERSION_200, VERSION_100,
                // From & to version:
                VERSION_100, VERSION_300,
                // Check result:
                false
        )
    }

    @Test fun checkUpdatedFromToAtAppSubsequentStartDuringSubsequentUpdate() {
        assertThatCheckUpdatedFromToReturnsWithResult(
                // Actual version:
                VERSION_300,
                // Persisted actual & previous version:
                VERSION_200, VERSION_100,
                // From & to version:
                VERSION_200, VERSION_300,
                // Check result:
                true
        )
        assertThatCheckUpdatedFromToReturnsWithResult(
                // Actual version:
                VERSION_300,
                // Persisted actual & previous version:
                VERSION_200, VERSION_100,
                // From & to version:
                VERSION_300, VERSION_300,
                // Check result:
                false
        )
    }

    @Test fun checkUpdatedFromToAtAppSubsequentStartAfterSubsequentUpdate() {
        assertThatCheckUpdatedFromToReturnsWithResult(
                // Actual version:
                VERSION_300,
                // Persisted actual & previous version:
                VERSION_300, VERSION_200,
                // From & to version:
                VERSION_200, VERSION_300,
                // Check result:
                true
        )
        assertThatCheckUpdatedFromToReturnsWithResult(
                // Actual version:
                VERSION_300,
                // Persisted actual & previous version:
                VERSION_300, VERSION_200,
                // From & to version:
                VERSION_300, VERSION_300,
                // Check result:
                false
        )
    }

    private fun assertThatCheckUpdatedFromToReturnsWithResult(
            actualVersion: Version,
            persistedVersionActual: Version,
            persistedVersionPrevious: Version,
            versionFrom: Version,
            versionTo: Version,
            result: Boolean
    ) {
        // Arrange:
        val mockRepository = createMockRepositoryWithVersions(persistedVersionActual, persistedVersionPrevious)
        val manager = VersionManager(mockRepository, actualVersion)
        // Act + Assert:
        assertThat(manager.checkUpdatedFromTo(versionFrom, versionTo),`is`(result))
    }

    @Test fun checkUpdatedAtAppFirstStart() {
        assertThatCheckUpdatedReturnsWithResult(
                // Actual version:
                VERSION_100,
                // Persisted actual & previous version:
                Version.EMPTY, Version.EMPTY,
                // Check result:
                false
        )
    }

    @Test fun checkUpdatedAtAppSubsequentStart() {
        assertThatCheckUpdatedReturnsWithResult(
                // Actual version:
                VERSION_100,
                // Persisted actual & previous version:
                VERSION_100, Version.EMPTY,
                // Check result:
                false
        )
    }

    @Test fun checkUpdatedAtAppSubsequentStartDuringFirstUpdate() {
        assertThatCheckUpdatedReturnsWithResult(
                // Actual version:
                VERSION_200,
                // Persisted actual & previous version:
                VERSION_100, Version.EMPTY,
                // Check result:
                true
        )
    }

    @Test fun checkUpdatedAtAppSubsequentStartAfterFirstUpdate() {
        assertThatCheckUpdatedReturnsWithResult(
                // Actual version:
                VERSION_200,
                // Persisted actual & previous version:
                VERSION_200, VERSION_100,
                // Check result:
                true
        )
    }

    @Test fun checkUpdatedAtAppSubsequentStartDuringSubsequentUpdate() {
        assertThatCheckUpdatedReturnsWithResult(
                // Actual version:
                VERSION_300,
                // Persisted actual & previous version:
                VERSION_200, VERSION_100,
                // Check result:
                true
        )
    }

    @Test fun checkUpdatedAtAppSubsequentStartAfterSubsequentUpdate() {
        assertThatCheckUpdatedReturnsWithResult(
                // Actual version:
                VERSION_300,
                // Persisted actual & previous version:
                VERSION_300, VERSION_200,
                // Check result:
                true
        )
    }

    private fun assertThatCheckUpdatedReturnsWithResult(
            actualVersion: Version,
            persistedVersionActual: Version,
            persistedVersionPrevious: Version,
            result: Boolean
    ) {
        // Arrange:
        val mockRepository = createMockRepositoryWithVersions(persistedVersionActual, persistedVersionPrevious)
        val manager = VersionManager(mockRepository, actualVersion)
        // Act + Assert:
        assertThat(manager.checkUpdated(),`is`(result))
    }

    private fun createMockRepositoryWithVersions(actualVersion: Version, previousVersion: Version): VersionRepository {
        val mockRepository = mock(VersionRepository::class.java)
        `when`(mockRepository.getActualVersion()).thenReturn(actualVersion)
        `when`(mockRepository.getPreviousVersion()).thenReturn(previousVersion)
        `when`(mockRepository.isEmpty()).thenReturn(Version.isEmpty(actualVersion) && Version.isEmpty(previousVersion))
        return mockRepository
    }
}