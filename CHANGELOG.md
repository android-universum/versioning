Change-Log
===============
> Regular configuration update: _30.05.2021_

More **detailed changelog** for each respective version may be viewed by pressing on a desired _version's name_.

## Version 2.x ##

### [2.0.0](https://bitbucket.org/android-universum/versioning/wiki/version/2.x) ###
> ??.??.2021

- New **group id**.

## [Version 1.x](https://bitbucket.org/android-universum/versioning/wiki/version/1.x) ##